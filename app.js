var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var app = express();


app.use(logger('dev'));
app.use(express.static('public'));
app.use(bodyParser.json({
    limit: '50mb'
}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true,
    parameterLimit: 50000
}));
app.use(bodyParser.json({
    type: 'application/json'
}));
app.use(bodyParser.text());

app.get('/',function(req,res){
	console.log(req.host);
	res.sendFile('public/index.html');
});

app.listen('3000','localhost',function(err){
	console.log(err ? err : "Server listening...!");
});